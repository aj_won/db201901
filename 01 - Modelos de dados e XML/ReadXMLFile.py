#!/usr/bin/python

from xml.dom.minidom import parse
import xml.dom.minidom
import csv

# Open XML document using minidom parser
DOMTree = xml.dom.minidom.parse("marvel_simplificado.xml")

universe = DOMTree.documentElement
if universe.hasAttribute("name"):
   print ("Root element : %s" % universe.getAttribute("name"))

# Get all the heroes in the universe
heroes = universe.getElementsByTagName("hero")


texto_final = ""
texto_good = ""
texto_bad = ""

num_herois = 0
num_bons = 0
num_maus = 0
peso = 0


# Print detail of each hero.
for hero in heroes:
   num_herois += 1
   if hero.hasAttribute("id"):
      texto = hero.getAttribute("id") + ", "

   name = hero.getElementsByTagName('name')[0]
   popularity = hero.getElementsByTagName('popularity')[0]
   alignment = hero.getElementsByTagName('alignment')[0]
   gender = hero.getElementsByTagName('gender')[0]
   height_m = hero.getElementsByTagName('height_m')[0]
   weight_kg = hero.getElementsByTagName('weight_kg')[0]
   hometown = hero.getElementsByTagName('hometown')[0]
   intelligence = hero.getElementsByTagName('intelligence')[0]
   strength = hero.getElementsByTagName('strength')[0]
   speed = hero.getElementsByTagName('speed')[0]
   durability = hero.getElementsByTagName('durability')[0]
   energy_Projection = hero.getElementsByTagName('energy_Projection')[0]
   fighting_Skills = hero.getElementsByTagName('fighting_Skills')[0]

   texto += name.childNodes[0].data + ", "
   texto += popularity.childNodes[0].data + ", "
   texto += alignment.childNodes[0].data + ", "
   texto += gender.childNodes[0].data + ", "
   texto += height_m.childNodes[0].data + ", "
   texto += weight_kg.childNodes[0].data + ", "
   texto += hometown.childNodes[0].data + ", "
   texto += intelligence.childNodes[0].data + ", "
   texto += strength.childNodes[0].data + ", "
   texto += speed.childNodes[0].data + ", "
   texto += durability.childNodes[0].data + ", "
   texto += energy_Projection.childNodes[0].data + ", "
   texto += fighting_Skills.childNodes[0].data + ", "
   texto += "\n"


   if alignment.childNodes[0].data == "Good":
      texto_good += texto
      num_bons += 1
   elif alignment.childNodes[0].data == "Bad":
      texto_bad += texto
      num_maus += 1
   texto_final += texto

   peso += int(weight_kg.childNodes[0].data)

   if int(hero.getAttribute("id")) == 3:
      imc = int(weight_kg.childNodes[0].data) / (int(height_m.childNodes[0].data)*int(height_m.childNodes[0].data))



proporcao = num_bons/num_maus
print ("Proporcao de herois bons/maus: %s" % proporcao)
media_peso = peso/num_herois
print ("Media de pesos: %s" % media_peso)
print ("IMC do hulk: %s" % imc)

with open('herois.csv', 'w') as file:
   file.write(texto_final)
with open('herois_good.csv', 'w') as file:
   file.write(texto_good)
with open('herois_bad.csv', 'w') as file:
   file.write(texto_bad)
