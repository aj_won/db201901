import psycopg2
import xml.etree.ElementTree as ET
import requests

class PostgreDB:
    def __init__(self):
        self.connection = psycopg2.connect(host='200.134.10.32', database='1901Whatever',
                                           user='1901Whatever', password='105254')
        self.cursor = self.connection.cursor()
        self.schema = 'exercicio4'

    def run(self, command):
        c = self.cursor
        try:
            c.execute(command)
        except Exception as e:
            print(e)

    def get(self, attrib):
        return self.cursor.fetchall(attrib)

    def commit(self):
        self.connection.commit()

    def close(self):
        self.connection.close()

    def create_tables(self):
        self.run("CREATE TABLE " + self.schema + '''.user(
                        link VARCHAR(200) PRIMARY KEY,
                        nome VARCHAR(100) NOT NULL,
                        cidade VARCHAR(30),
                        aniversario DATE);''')
        self.run("CREATE TABLE " + self.schema + '''.friends(
                        user1 VARCHAR(200) NOT NULL,
                        user2 VARCHAR(200) NOT NULL,
                        FOREIGN KEY (user1) REFERENCES exercicio4.user(link) ON DELETE CASCADE ON UPDATE CASCADE,
                        FOREIGN KEY (user2) REFERENCES exercicio4.user(link) ON DELETE CASCADE ON UPDATE CASCADE,
                        CONSTRAINT user_constraint UNIQUE(user1, user2));''')
        self.run("CREATE TABLE " + self.schema + '''.likes_music(
                        user1 VARCHAR(200) NOT NULL,
                        band_link VARCHAR(200),
                        rating INTEGER,
                        FOREIGN KEY (user1) REFERENCES exercicio4.user(link) ON DELETE CASCADE ON UPDATE CASCADE);''')
        self.run("CREATE TABLE " + self.schema + '''.likes_movie(
                        user1 VARCHAR(200) NOT NULL,
                        movie_link VARCHAR(200),
                        rating INTEGER,
                        FOREIGN KEY (user1) REFERENCES exercicio4.user(link) ON DELETE CASCADE ON UPDATE CASCADE);''')

    def drop_tables(self):
        self.run('DROP TABLE IF EXISTS ' + self.schema + '.user CASCADE;')
        self.run('DROP TABLE IF EXISTS ' + self.schema + '.likes_movie CASCADE;')
        self.run('DROP TABLE IF EXISTS ' + self.schema + '.likes_music CASCADE;')
        self.run('DROP TABLE IF EXISTS ' + self.schema + '.friends CASCADE')

    def insert(self, table, values):
        print(table, values)
        if table == 'Person':
            if values['birthdate'] == '':
                values['birthdate'] = '1900-01-01'
            if values['hometown'] == '':
                values['hometown'] = 'Lugar Nenhum'
            cmd_str = '''INSERT INTO '''+self.schema+'''.user VALUES ('{}', '{}', '{}', '{}');'''.format(values['uri'],
                                                                                                         values['name'],
                                                                                                         values['hometown'],
                                                                                                         values['birthdate'])
        elif table == 'LikesMusic':
            cmd_str = '''INSERT INTO '''+self.schema+'''.likes_music VALUES ('{}', '{}', {});'''.format(values['person'],
                                                                                                          values['bandUri'],
                                                                                                          values['rating'])
        elif table == 'LikesMovie':
            cmd_str = '''INSERT INTO '''+self.schema+'''.likes_movie VALUES ('{}', '{}', {});'''.format(values['person'],
                                                                                                          values['movieUri'],
                                                                                                          values['rating'])
        elif table == 'Knows':
            cmd_str = '''INSERT INTO '''+self.schema+'''.friends VALUES ('{}', '{}');'''.format(values['person'],
                                                                                                values['colleague'])
        else:
            print("O programa não está preparado para gerenciar a seguinte tabela:", table)
            raise Exception

        self.run(cmd_str)


class XMLParser:
    def parse(self):
        root = []
        links = ["http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/person.xml",
                 "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/music.xml",
                 "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/movie.xml",
                 "http://dainf.ct.utfpr.edu.br/~gomesjr/BD1/data/knows.xml"]
        for link in links:
            print("Reading", link)
            response = requests.get(link)
            root.append(ET.fromstring(response.content))
        return root


def print_list(content):
    for item in content:
        print(item)
        for key, value in item.attrib.items():
            print('\t', key + ":", value)


db = PostgreDB()
root = XMLParser().parse()

db.drop_tables()
db.create_tables()

tables = ["Person", "LikesMusic", "LikesMovie", "Knows"]
for root, table in zip(root, tables):
    lista = root.findall(table)
    for item in lista:
        db.insert(table, item.attrib)

db.commit()











