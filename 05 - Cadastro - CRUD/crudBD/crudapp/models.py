from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.


class User(models.Model):
    class Meta:
        db_table = 'user'
    link = models.CharField(max_length=200, null=True)
    nome = models.CharField(max_length=100, null=True)
    cidade = models.CharField(max_length=30, null=True)
    aniversario = models.CharField(max_length=10, default='12/31/1900')


class Friends(models.Model):
    class Meta:
        db_table = 'friends'
        unique_together = (('user1', 'user2'),)
    user1 = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    user2 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friend', null=True)


class LikesMovie(models.Model):
    class Meta:
        db_table = 'likes_movie'

    user1 = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    movie_link = models.CharField(max_length=200, null=True)
    rating = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)])


class LikesMusic(models.Model):
    class Meta:
        db_table = 'likes_music'

    user1 = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    band_link = models.CharField(max_length=200, null=True)
    rating = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)])

