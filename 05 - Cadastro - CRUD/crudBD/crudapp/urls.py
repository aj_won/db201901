
from django.urls import path, include
from . import views

app_name = 'crudapp'
urlpatterns = [
    # Páginas de visualização
    path('', views.MainPageView.as_view(), name='main_page'),
    path('user_list/', views.UserListView.as_view(), name='user_list_page'),
    path('new_user/', views.NewUserView.as_view(), name='new_user_page'),
    path('edit_user/<slug:user_pk>/', views.EditUserView.as_view(), name='edit_user_page'),
    path('delete_user/<slug:user_pk>/', views.DeleteUserView.as_view(), name='delete_user_page'),
    path('add_friends/<slug:user_pk>/', views.AddFriendView.as_view(), name='add_friend_page'),
    path('add_friend/<slug:user_pk>/<slug:user_pk2>/', views.add_friend, name='add_friend'),
    path('add_movie/<slug:user_pk>/', views.AddMovieView.as_view(), name='add_movie_page'),
    path('add_music/<slug:user_pk>/', views.AddMusicView.as_view(), name='add_music_page'),
    path('data/',views.Data.as_view(), name = 'data'),

    # Para importar tabelas csv nos bancos (não tem acesso pela interface)
    path('import_db_users/', views.import_db_users, name='import_db_users'),
    path('import_db_friends/', views.import_db_friends, name='import_db_friends'),
    path('import_db_likes_movie/', views.import_db_likes_movie, name='import_db_likes_movie'),
    path('import_db_likes_music/', views.import_db_likes_music, name='import_db_likes_music'),
]
