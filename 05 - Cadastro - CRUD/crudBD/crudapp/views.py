from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from .models import User, Friends
from .forms import EditUserForm
from .resources import *
from tablib import Dataset



class Data(View):
    template_name = 'crudapp/data.html'
    def get_media_music(self):
        list_music=LikesMusic.objects.filter()
        nota =0
        i =0
        for music in list_music:
            nota = nota+music.rating
            i = i+1
        return nota/i

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['music_list'] = LikesMusic.objects.filter()
        context['media'] = self.get_media_music()
        return context

class UserListView(ListView):
    model = User
    template_name = 'crudapp/list_users.html'
    ordering = ['nome']


class NewUserView(CreateView):
    model = User
    fields = '__all__'
    template_name = 'crudapp/form_user.html'
    success_url = reverse_lazy('crudapp:main_page')


class MainPageView(View):
    def get(self, request):
        return render(request, 'crudapp/main_page.html', {})


class EditUserView(UpdateView):
    model = User
    form_class = EditUserForm
    template_name = 'crudapp/edit_user.html'
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'
    success_url = reverse_lazy('crudapp:user_list_page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['music_list'] = LikesMusic.objects.filter(user1=self.object.pk)
        context['movie_list'] = LikesMovie.objects.filter(user1=self.object.pk)
        return context



class DeleteUserView(DeleteView):
    model = User
    success_url = reverse_lazy('crudapp:user_list_page')
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'


class AddFriendView(View):
    def get(self, request, user_pk):
        all_users = User.objects.all().order_by('nome')
        try:
            friends = [friendship.user2.pk for friendship in Friends.objects.filter(user1=user_pk)]
            friends = all_users.filter(pk__in=friends).order_by('nome')
        except Friends.DoesNotExist:
            friends = []
        user = all_users.get(pk=user_pk)
        not_friends = []
        for other_user in all_users:
            if other_user.link != user.link and other_user not in friends:
                not_friends.append(other_user)
        user = all_users.get(pk=user_pk)
        return render(request, 'crudapp/add_friend.html', {'user': user, 'friends': friends, 'not_friends': not_friends})


class AddMusicView(CreateView):
    model = LikesMusic
    fields = ('band_link', 'rating')
    template_name = 'crudapp/add_music.html'

    def get(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        return super().post(request)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        form.save()
        return redirect('crudapp:edit_user_page', user_pk=self.user_pk)


class AddMovieView(CreateView):
    model = LikesMovie
    fields = ('movie_link', 'rating')
    template_name = 'crudapp/add_movie.html'

    def get(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        return super().post(request)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        form.save()
        return redirect('crudapp:edit_user_page', user_pk=self.user_pk)


def add_friend(request, user_pk, user_pk2):
    user1 = User.objects.get(pk=user_pk)
    user2 = User.objects.get(pk=user_pk2)
    Friends.objects.create(user1=user1, user2=user2)
    return redirect('crudapp:add_friend_page', user_pk=user_pk)




def import_db_users(request):
    if request.method == 'POST':
        user_resource = UserResource()

        new_users = request.FILES['myfile']
        s = new_users.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido
            dataset.append(s_list[ind])

        result = user_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            user_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_friends(request):
    if request.method == 'POST':
        friends_resource = FriendsResource()
        new_friends = request.FILES['myfile']
        s = new_friends.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
                s_list[ind][1] = User.objects.get(link=entry[1]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = friends_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            friends_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_movie(request):
    if request.method == 'POST':
        movie_resource = LikesMovieResource()
        new_movies = request.FILES['myfile']
        s = new_movies.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = movie_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            movie_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_music(request):
    if request.method == 'POST':
        music_resource = LikesMusicResource()
        new_music = request.FILES['myfile']
        s = new_music.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = music_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            music_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')