from bs4 import BeautifulSoup
import requests
import xml.etree.cElementTree as ET
import pylast

API_KEY = " 3541beb60a5d0392d370ea327d89b9ef"
API_SECRET = "a7ddf636ab7380cda11ba8a975d5287e"
#----------------------------------------FILMES----------------------------------------------#

url = 'http://www.imdb.com/title/tt0245429/'
resposta = requests.get(url)
soup = BeautifulSoup(resposta.text, features="html.parser")


def retornaNome(soup):
    body = soup.find('body', {'class': 'fixed'})

    seila = body.find('div', {'id': 'wrapper'})
    seila2 = seila.find('div', {'id': 'root'}, {'class': 'redesign'})
    seila3 = seila2.find('div', {'id': 'pagecontent'}, {'class': 'pagecontent'})
    seila4 = seila3.find('div', {'id': 'content-2-wide'}, {'class': 'flatland'})
    seila5 = seila4.find('div', {'id': 'main_top'}, {'class': 'main'})
    seila6 = seila5.find('div', {'class': 'title-overview'})
    seila7 = seila6.find('div', {'id': 'title-overview-widget'}, {'class': 'heroic-overview'})
    seila8 = seila7.find('div', {'class': 'vital'})
    seila9 = seila8.find('div', {'class': 'title_block'})
    seila10 = seila9.find('div', {'class': 'title_bar_wrapper'})
    seila11 = seila10.find('div', {'class': 'title_wrapper'})
    finalmente = seila11.find('h1', {'class': ''}).text

    return finalmente


def retornaAno(soup):
    body = soup.find('body', {'class': 'fixed'})

    seila = body.find('div', {'id': 'wrapper'})
    seila2 = seila.find('div', {'id': 'root'}, {'class': 'redesign'})
    seila3 = seila2.find('div', {'id': 'pagecontent'}, {'class': 'pagecontent'})
    seila4 = seila3.find('div', {'id': 'content-2-wide'}, {'class': 'flatland'})
    seila5 = seila4.find('div', {'id': 'main_top'}, {'class': 'main'})
    seila6 = seila5.find('div', {'class': 'title-overview'})
    seila7 = seila6.find('div', {'id': 'title-overview-widget'}, {'class': 'heroic-overview'})
    seila8 = seila7.find('div', {'class': 'vital'})
    seila9 = seila8.find('div', {'class': 'title_block'})
    seila10 = seila9.find('div', {'class': 'title_bar_wrapper'})
    seila11 = seila10.find('div', {'class': 'title_wrapper'})
    seila12 = seila11.find('div', {'class': 'subtext'})
    finalmente = seila12.find('a', {'title': 'See more release dates'}).text
    return finalmente


def retornaGenero(soup):
    body = soup.find('body', {'class': 'fixed'})

    seila = body.find('div', {'id': 'wrapper'})
    seila2 = seila.find('div', {'id': 'root'}, {'class': 'redesign'})
    seila3 = seila2.find('div', {'id': 'pagecontent'}, {'class': 'pagecontent'})
    seila4 = seila3.find('div', {'id': 'content-2-wide'}, {'class': 'flatland'})
    seila5 = seila4.find('div', {'id': 'main_top'}, {'class': 'main'})
    seila6 = seila5.find('div', {'class': 'title-overview'})
    seila7 = seila6.find('div', {'id': 'title-overview-widget'}, {'class': 'heroic-overview'})
    seila8 = seila7.find('div', {'class': 'vital'})
    seila9 = seila8.find('div', {'class': 'title_block'})
    seila10 = seila9.find('div', {'class': 'title_bar_wrapper'})
    seila11 = seila10.find('div', {'class': 'title_wrapper'})
    seila12 = seila11.find('div', {'class': 'subtext'})
    finalmente = seila12.find('a').text
    return finalmente


def retornaDiretor(soup):
    body = soup.find('body', {'class': 'fixed'})

    seila = body.find('div', {'id': 'wrapper'})
    seila2 = seila.find('div', {'id': 'root'}, {'class': 'redesign'})
    seila3 = seila2.find('div', {'id': 'pagecontent'}, {'class': 'pagecontent'})
    seila4 = seila3.find('div', {'id': 'content-2-wide'}, {'class': 'flatland'})
    seila5 = seila4.find('div', {'id': 'main_top'}, {'class': 'main'})
    seila6 = seila5.find('div', {'class': 'title-overview'})
    seila7 = seila6.find('div', {'id': 'title-overview-widget'}, {'class': 'heroic-overview'})
    seila8 = seila7.find('div', {'class': 'plot_summary_wrapper'})
    seila9 = seila8.find('div', {'class': 'plot_summary'})
    seila10 = seila9.find('div', {'class': 'credit_summary_item'})
    finalmente = seila10.find('a').text
    return finalmente

#Arquivo xml --------------------------------------------------------------

root = ET.Element('root')
filme = ET.SubElement(root, "Filme")
ano = ET.SubElement(filme, "Data")
genero = ET.SubElement(filme, "Genero")
diretor = ET.SubElement(filme, "Diretor")

filme.text = retornaNome(soup)
ano.text = retornaAno(soup)
genero.text = retornaGenero(soup)
diretor.text = retornaDiretor(soup)

tree = ET.ElementTree(root)
tree.write("movie.xml")

print(retornaNome(soup))
print(retornaAno(soup))
print(retornaGenero(soup))
print(retornaDiretor(soup))


#----------------------------------------MUSIC----------------------------------------------#

username = "aj_won"
password_hash = pylast.md5("P4ssW0rd*")

url_wiki = 'https://en.wikipedia.org/wiki/Young_the_Giant'

#Wikipedia------------------------------------------------------------------------
resposta = requests.get(url_wiki)
soup_music = BeautifulSoup(resposta.text, features="html.parser")

artist_name = soup_music.find_all('h1', class_='firstHeading')[0].get_text()

network = pylast.LastFMNetwork(api_key=API_KEY, api_secret=API_SECRET,
                               username=username, password_hash=password_hash)

artist = network.get_artist(artist_name)

#Generos--------------------------------------------------------------------
TopItens = artist.get_top_tags(limit=3)
tags = ""
for topItem in TopItens:
    tags = tags + topItem.item.get_name() + ", "

#URL-------------------------------------------------------------------------
url = artist.get_url()
resposta = requests.get(url)
soup_music = BeautifulSoup(resposta.text, features="html.parser")



#Arquivo xml --------------------------------------------------------------
root1 = ET.Element('root')
artista = ET.SubElement(root1,"Artista")
cep = ET.SubElement(artista, "Origem")
generos = ET.SubElement(artista,"Generos")
listeners = ET.SubElement(artista,"Listeners")

artista.text = artist_name
cep.text = soup_music.find_all('p', class_='factbox-summary')[0].get_text()
generos.text = tags
listeners.text = str(artist.get_listener_count())

tree = ET.ElementTree(root1)
tree.write("music.xml")