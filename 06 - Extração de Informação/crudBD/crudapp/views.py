from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.template.loader import get_template
from django.http import HttpResponse
from .models import User, Friends
from .forms import EditUserForm
from .resources import *
from tablib import Dataset
from django.db.models import Avg, StdDev
from django.db import connection
from matplotlib import pyplot as plt
import numpy as np
from math import pi
import base64, io
import csv


from bs4 import BeautifulSoup
import requests
import xml.etree.cElementTree as ET
import pylast

API_KEY = " 3541beb60a5d0392d370ea327d89b9ef"
API_SECRET = "a7ddf636ab7380cda11ba8a975d5287e"


class UserListView(ListView):
    model = User
    template_name = 'crudapp/list_users.html'
    ordering = ['nome']


class NewUserView(CreateView):
    model = User
    fields = '__all__'
    template_name = 'crudapp/form_user.html'
    success_url = reverse_lazy('crudapp:main_page')


class MainPageView(View):
    def get(self, request):
        return render(request, 'crudapp/main_page.html', {})


class EditUserView(UpdateView):
    model = User
    form_class = EditUserForm
    template_name = 'crudapp/edit_user.html'
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'
    success_url = reverse_lazy('crudapp:user_list_page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['music_list'] = LikesMusic.objects.filter(user1=self.object.pk)
        context['movie_list'] = LikesMovie.objects.filter(user1=self.object.pk)
        return context


class DeleteUserView(DeleteView):
    model = User
    success_url = reverse_lazy('crudapp:user_list_page')
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'


class AddFriendView(View):
    def get(self, request, user_pk):
        all_users = User.objects.all().order_by('nome')
        try:
            friends = [friendship.user2.pk for friendship in Friends.objects.filter(user1=user_pk)]
            friends = all_users.filter(pk__in=friends).order_by('nome')
        except Friends.DoesNotExist:
            friends = []
        user = all_users.get(pk=user_pk)
        not_friends = []
        for other_user in all_users:
            if other_user.link != user.link and other_user not in friends:
                not_friends.append(other_user)
        user = all_users.get(pk=user_pk)
        return render(request, 'crudapp/add_friend.html', {'user': user, 'friends': friends, 'not_friends': not_friends})


class AddMusicView(CreateView):
    model = LikesMusic
    fields = ('band_link', 'rating')
    template_name = 'crudapp/add_music.html'

    def get(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        return super().post(request)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        form.save()
        return redirect('crudapp:edit_user_page', user_pk=self.user_pk)


class AddMovieView(CreateView):
    model = LikesMovie
    fields = ('movie_link', 'rating')
    template_name = 'crudapp/add_movie.html'

    def get(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        return super().post(request)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        form.save()
        return redirect('crudapp:edit_user_page', user_pk=self.user_pk)


def add_friend(request, user_pk, user_pk2):
    user1 = User.objects.get(pk=user_pk)
    user2 = User.objects.get(pk=user_pk2)
    Friends.objects.create(user1=user1, user2=user2)
    return redirect('crudapp:add_friend_page', user_pk=user_pk)

def import_db_users(request):
    if request.method == 'POST':
        user_resource = UserResource()

        new_users = request.FILES['myfile']
        s = new_users.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido
            dataset.append(s_list[ind])

        result = user_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            user_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_friends(request):
    if request.method == 'POST':
        friends_resource = FriendsResource()
        new_friends = request.FILES['myfile']
        s = new_friends.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
                s_list[ind][1] = User.objects.get(link=entry[1]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = friends_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            friends_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_movie(request):
    if request.method == 'POST':
        movie_resource = LikesMovieResource()
        new_movies = request.FILES['myfile']
        s = new_movies.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = movie_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            movie_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_music(request):
    if request.method == 'POST':
        music_resource = LikesMusicResource()
        new_music = request.FILES['myfile']
        s = new_music.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = music_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            music_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')


class StatisticsPageView(View):
    def get(self, request):
        return render(request, 'crudapp/statistics_page.html')


class AveragePageView(View):
    def get(self, request):
        ##### item 1 artistas #####
        notas_music = LikesMusic.objects.all().aggregate(Avg('rating'))['rating__avg']
        desvio_music = LikesMusic.objects.all().aggregate(StdDev('rating'))['rating__stddev']

        ##### item 1 filmes #####
        notas_films = LikesMovie.objects.all().aggregate(Avg('rating'))['rating__avg']
        desvio_films = LikesMovie.objects.all().aggregate(StdDev('rating'))['rating__stddev']

        return render(request, 'crudapp/average_page.html',
                      {'notas_music':notas_music, 'desvio_music':desvio_music,
                       'notas_films':notas_films, 'desvio_films':desvio_films})


class RatingsPageView(View):
    def get(self, request):
        ##### item 2 artistas #####
        info_music = []  # lista de dicionarios
        all_artistas = LikesMusic.objects.all()
        for artista in all_artistas:
            jafoi = False
            for item in info_music:
                if artista.band_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_artistas.filter(band_link=artista.band_link)
            if cada_um.count() > 1:
                rating = cada_um.aggregate(Avg('rating'))['rating__avg']
                info_music.append({'link': artista.band_link, 'rating': rating})
                info_music.sort(key=lambda x: x['rating'], reverse=True)

        ##### item 1 filmes #####
        info_films = []
        all_films = LikesMovie.objects.all()
        for movie in all_films:
            jafoi = False
            for item in info_films:
                if movie.movie_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_films.filter(movie_link=movie.movie_link)
            if cada_um.count() > 1:
                rating = cada_um.aggregate(Avg('rating'))['rating__avg']
                info_films.append({'link': movie.movie_link, 'rating': rating})
                info_films.sort(key=lambda x: x['rating'], reverse=True)

        return render(request, 'crudapp/ratings_page.html',
                  {'tabela_music': info_music,
                   'tabela_films': info_films,})


class PopularityPageView(View):
    def get(self, request):
    ##### item 3 #####
        info_populares_musica = []
        all_artistas = LikesMusic.objects.all()
        for artista in all_artistas:
            jafoi = False
            for item in info_populares_musica:
                if artista.band_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_artistas.filter(band_link=artista.band_link)
            count = cada_um.count()
            info_populares_musica.append({'link': artista.band_link, 'count': count})
            info_populares_musica.sort(key=lambda x: x['count'], reverse=True)
        info_populares_musica = info_populares_musica[0:10]

    ##### item 3 filmes #####
        info_populares_films = []
        all_movies = LikesMovie.objects.all()
        for movie in all_movies:
            jafoi = False
            for item in info_populares_films:
                if movie.movie_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_movies.filter(movie_link=movie.movie_link)
            count = cada_um.count()
            info_populares_films.append({'link': movie.movie_link, 'count': count})
            info_populares_films.sort(key=lambda x: x['count'], reverse=True)
        info_populares_films = info_populares_films[0:10]

        return render(request, 'crudapp/popularity_page.html', {'populares_musica':info_populares_musica,'populares_filmes':info_populares_films})


class ConhecidosPageView(View):
    def get(self, request):
        ##### item 5 #####
        cursor = connection.cursor()

        sql_string = 'SELECT cn.user1, cn.user2 FROM crud.conhece_normalizada cn ' \
                     'WHERE (SELECT COUNT(cf1.movie_link) FROM crud.likes_movie cf1, crud.likes_movie cf2, crud.conhece_normalizada cn2 ' \
                     'WHERE cn.user1 = cf1.user1_id AND cn.user2=cf2.user1_id AND cf1.movie_link = cf2.movie_link)>0 ' \
                     'ORDER by (SELECT COUNT(cf1.movie_link) FROM crud.likes_movie cf1, crud.likes_movie cf2, crud.conhece_normalizada cn' \
                     ' WHERE cn.user1=cf1.user1_id AND cn.user2=cf2.user1_id AND cf1.movie_link=cf2.movie_link) DESC LIMIT 1'
        cursor.execute(sql_string)
        users_films = cursor.fetchall()
        users_films = User.objects.filter(pk__in=users_films[0])

        ##### item 6 #####

        sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
                     'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 42)'
        cursor.execute(sql_string)
        count_con_con_A = cursor.fetchall()[0][0]
        sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
                     'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 47)'
        cursor.execute(sql_string)
        count_con_con_C = cursor.fetchall()[0][0]
        sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
                     'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 62)'
        cursor.execute(sql_string)
        count_con_con_J = cursor.fetchall()[0][0]

        return render(request, 'crudapp/conhecidos_page.html',
                      {'users_films': users_films, 'conhecidos_conhecidos_A': count_con_con_A,
                       'conhecidos_conhecidos_C': count_con_con_C, 'conhecidos_conhecidos_J': count_con_con_J})


class Graphics_1PageView(View):
    def get(self, request):

        ##### item 7 #####
        cursor = connection.cursor()
        sql_string = 'SELECT COUNT(lm.user1_id) FROM crud.likes_movie lm GROUP BY lm.user1_id ORDER BY COUNT(lm.user1_id) DESC '
        cursor.execute(sql_string)
        qtd = cursor.fetchall()
        z=[0]*20
        for i in range(0,len(qtd)):
            a=qtd[i][0]
            z[a]=z[a]+1

        x = np.arange(0,len(z), 1)  # Domínio da função - [start, end, step]
        y = z   # Função
        fig, ax = plt.subplots()
        plt.plot(x, y)
        ax.set(xlabel='N de pessoas', ylabel='Filmes curtidos', title="Item 7")
        ax.grid()
        #plt.show()  # Descomentem pra ver o gráfico fora do site

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img3 = base64.b64encode(buf.getvalue())
        img3 = img3.decode()
        result = get_template('crudapp/graphics_1_page.html').render({'img3': img3})
        return HttpResponse(result)


class GraphicsPageView(View):
    def get(self, request):

        ##### item 8 #####

        contagens_filmes = []
        all_filmes = LikesMovie.objects.all()
        for filme in all_filmes:
            cada_filme = all_filmes.filter(movie_link=filme.movie_link)
            contagens_filmes.append(cada_filme.count())

        eixo_y = []
        for x in range(len(contagens_filmes)):
            quantidade = contagens_filmes.count(contagens_filmes[x])
            eixo_y.append(quantidade)
        sem_repetidos = []
        for i in eixo_y:
            if i not in sem_repetidos:
                sem_repetidos.append(i)
        sem_repetidos.sort()

        x = np.arange(0, 12, 1)  # Domínio da função - [start, end, step]
        y = sem_repetidos  # Função
        fig, ax = plt.subplots()
        plt.plot(x, y)
        ax.set(xlabel='Pessoas', ylabel='Número de filmes', title="Numero de filmes curtidos por x pessoas")
        ax.grid()
        # plt.show()  # Descomentem pra ver o gráfico fora do site

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img = base64.b64encode(buf.getvalue())
        img = img.decode()




        # conhecem = {}
        # users = User.objects.all()
        # max_friends = 0
        # for user in users:
        #     number_of_friends = len(Friends.objects.filter(user1=user.pk))
        #     if number_of_friends > max_friends:
        #         max_friends = number_of_friends
        #
        # for i in range(max_friends+1):
        #     conhecem.update({i: 0})
        #
        # for user in users:
        #     number_of_friends = len(Friends.objects.filter(user1=user.pk))
        #     conhecem[number_of_friends] += 1

        # print(conhecem)
        # x = [value for value in conhecem.values()]
        # print(x)
        # # x = x   # Domínio da função - [start, end, step]
        # y = conhecem.keys()  # Função
        # fig, ax = plt.subplots()
        # plt.plot(x, y)
        # ax.set(xlabel='Número de pessoas', ylabel='Número de Amigos', title="Número de pessoas x número de amigos")
        # ax.grid()
        # # plt.show()  # Descomentem pra ver o gráfico fora do site
        #
        # buf = io.BytesIO()
        # plt.savefig(buf, format='png')
        # plt.close(fig)
        # img2 = base64.b64encode(buf.getvalue())
        # img2 = img2.decode()

        result = get_template('crudapp/graphics_page.html').render({'img': img})



        return HttpResponse(result)


class XMLGenerator(View):
    def get(self, request, xml_type):
        print("\nIsso vai demorar bastante. Você será redirecionado quando tudo terminar.")
        if xml_type == "music":
            print("Gerando XML das músicas.")
            self.get_music()
        elif xml_type == "movie":
            print("Gerando XML dos filmes.")
            self.get_movies()
        else:
            print("ERRO: 'type' precisa ser 'music' ou 'movie'.")

        return redirect('crudapp:main_page')

    def get_movies(self):
        movies = LikesMovie.objects.all()
        root = ET.Element('root')
        total = len(movies)
        for ind, movie in enumerate(movies):
            print("Processando filmes:", ind+1, "de", str(total) + ".", end="\r", flush=True)
            url = movie.movie_link
            resposta = requests.get(url)
            soup = BeautifulSoup(resposta.text, features="html.parser")

            filme = ET.SubElement(root, "Filme")
            ano = ET.SubElement(filme, "Data")
            genero = ET.SubElement(filme, "Genero")
            diretor = ET.SubElement(filme, "Diretor")

            filme.text = self.retornaNome(soup)
            ano.text = self.retornaAno(soup)
            genero.text = self.retornaGenero(soup)
            diretor.text = self.retornaDiretor(soup)

        tree = ET.ElementTree(root)
        tree.write("movie.xml")


    def get_music(self):
        username = "aj_won"
        password_hash = pylast.md5("P4ssW0rd*")
        root1 = ET.Element('root')
        musics = LikesMusic.objects.all()
        total = len(musics)
        for ind, music in enumerate(musics):
            print("Processando musicas:", ind + 1, "de", str(total) + ".", end="\r", flush=True)
            url_wiki = music.band_link
            resposta = requests.get(url_wiki)
            soup_music = BeautifulSoup(resposta.text, features="html.parser")

            artist_name = soup_music.find('h1', class_='firstHeading').text

            achou = artist_name.find("(")
            if achou != -1:
                artist_name = artist_name[:achou - 1]

            network = pylast.LastFMNetwork(api_key=API_KEY, api_secret=API_SECRET,
                                           username=username, password_hash=password_hash)

            artist = network.get_artist(artist_name)

            # Generos--------------------------------------------------------------------
            TopItens = artist.get_top_tags(limit=3)
            tags = ""
            for topItem in TopItens:
                tags = tags + topItem.item.get_name() + ", "


            # URL-------------------------------------------------------------------------
            url = artist.get_url()
            resposta = requests.get(url)
            soup_music = BeautifulSoup(resposta.text, features="html.parser")

            # Arquivo xml --------------------------------------------------------------

            artista = ET.SubElement(root1, "Artista")
            cep = ET.SubElement(artista, "Origem")
            generos = ET.SubElement(artista, "Generos")
            listeners = ET.SubElement(artista, "Listeners")

            artista.text = artist_name
            if artist_name != 'Nirvana' and artist_name !=  'Drake':
                cep.text = soup_music.find('p', class_='factbox-summary').text
            generos.text = tags
            listeners.text = str(artist.get_listener_count())

        tree = ET.ElementTree(root1)
        tree.write("music.xml")


    def retornaNome(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            if current_info is None:
                return
            # print(current_info.encode("utf-8"), type(current_info))

        finalmente = current_info.find('h1', {'class': ''})
        if finalmente is None:
            return

        finalmente = finalmente.text
        return finalmente

    def retornaAno(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},),
                 ({'class': 'subtext'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a', {'title': 'See more release dates'}).text
        return finalmente

    def retornaGenero(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},),
                 ({'class': 'subtext'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a').text
        return finalmente

    def retornaDiretor(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'plot_summary_wrapper'},),
                 ({'class': 'plot_summary'},),
                 ({'class': 'credit_summary_item'},),
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a').text
        return finalmente


