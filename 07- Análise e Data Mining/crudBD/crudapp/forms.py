from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.forms import ModelForm, Textarea, CharField, TextInput, DateInput, DateField, Form, BooleanField, ModelChoiceField, ChoiceField, MultipleChoiceField, CheckboxSelectMultiple
from django.contrib.auth import get_user_model
from .models import LikesMovie,LikesMusic
User = get_user_model()


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'hometown', 'birthday', 'profile_pic')


class EditUserForm(UserForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['readonly'] = True

    def clean_link(self):
        instance = getattr(self, 'instance', None)
        if instance:
            return instance.link
        else:
            return self.cleaned_data.get('link', None)


class LoginForm(AuthenticationForm):
    class Meta:
        model = User
        fields = ['username', 'password']


class SignUpForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'hometown', 'birthday', 'password1', 'password2',
                  'profile_pic')
        labels = {'profile_pic': 'Profile Picture', }


class AddMovieForm(ModelForm):
    movie_link = CharField(required=True)

    class Meta:
        model = LikesMovie
        fields = ('rating', )

class AddMusicForm(ModelForm):
    music_name = CharField(required=True)

    class Meta:
        model = LikesMusic
        fields = ('rating', )
