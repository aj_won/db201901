from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User as DjangoUser
from django.urls import reverse
import os
from uuid import uuid4


def path_and_rename(instance, filename):
    upload_to = ''
    ext = filename.split('.')[-1]
    # get filename
    if instance.pk:
        filename = '{}.{}'.format(instance.username, ext)
    else:
        # set filename as random string
        filename = '{}.{}'.format(uuid4().hex, ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)


class User(AbstractUser):
    class Meta:
        db_table = 'user'
    hometown = models.CharField(max_length=30, null=True)
    birthday = models.CharField(max_length=10, default='12/31/1900')
    profile_pic = models.ImageField(upload_to=path_and_rename, default='no_pic.jpg')

    def get_absolute_url(self):
        return reverse('crudapp:user_page', kwargs={'username': self.username})


class Friends(models.Model):
    class Meta:
        db_table = 'friends'
        unique_together = (('user1', 'user2'),)
    user1 = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    user2 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friend', null=True)


class Movie(models.Model):
    class Meta:
        db_table = 'movie'

    link = models.CharField(max_length=200, null=True)
    nome = models.CharField(max_length=200, null=True)
    lancamento = models.CharField(max_length=50, null=True)
    diretor = models.CharField(max_length=200, null=True)
    genero = models.CharField(max_length=200, null=True)
    img_link = models.CharField(max_length=500, null=True)


class Music(models.Model):
    class Meta:
        db_table = 'music'

    nome = models.CharField(max_length=200, null=True)
    seguidores = models.CharField(max_length=50, null=True)
    genero = models.CharField(max_length=200, null=True)
    spotify = models.CharField(max_length=200, null=True)
    img_link = models.CharField(max_length=500, null=True)


class LikesMovie(models.Model):
    class Meta:
        db_table = 'likes_movie'
        unique_together = ('user1', 'movie')

    user1 = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, null=True)
    rating = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)])


class LikesMusic(models.Model):
    class Meta:
        db_table = 'likes_music'
        unique_together = ('user1', 'music')

    user1 = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    music = models.ForeignKey(Music, on_delete=models.CASCADE, null=True)
    rating = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(5)])



