from django import template

register = template.Library()


@register.simple_tag
def print_value(value):
    print("VALUE:", value)
    return value


register.filter('print_value', print_value)