
from django.urls import path, include
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'crudapp'




urlpatterns = [
    # Páginas de visualização
    path('', views.MainPageView.as_view(), name='main_page'),
    path('user_list/', views.UserListView.as_view(), name='user_list_page'),
    path('new_user/', views.NewUserView.as_view(), name='new_user_page'),
    path('user_page/<slug:username>/', views.ShowUserView.as_view(), name='user_page'),
    path('edit_user/<slug:user_pk>/', views.EditUserView.as_view(), name='edit_user_page'),
    path('delete_user/<slug:user_pk>/', views.DeleteUserView.as_view(), name='delete_user_page'),
    path('add_friends/<slug:user_pk>/', views.AddFriendView.as_view(), name='add_friend_page'),
    path('add_friend/<slug:user_pk>/<slug:user_pk2>/', views.add_friend, name='add_friend'),
    path('add_movie/<slug:user_pk>/', views.AddMovieView.as_view(), name='add_movie_page'),
    path('add_movie/<slug:user_pk>/<slug:movie_pk>/', views.AddMovieView.as_view(), name='add_movie_page'),
    path('add_music/<slug:user_pk>/', views.AddMusicView.as_view(), name='add_music_page'),
    path('add_music/<slug:user_pk>/<slug:music_pk>/', views.AddMusicView.as_view(), name='add_music_page'),
    path('statistics/', views.StatisticsPageView.as_view(), name='statistics_page'),
    path('popularity/', views.PopularityPageView.as_view(), name='popularity_page'),
    path('statistics/average', views.AveragePageView.as_view(), name='average_page'),
    path('statistics/ratings', views.RatingsPageView.as_view(), name='ratings_page'),
    path('statistics/conhecidos', views.ConhecidosPageView.as_view(), name='conhecidos_page'),
    path('statistics/graphics', views.GraphicsPageView.as_view(), name='graphics_page'),
    path('media/<slug:image_name>', views.GraphicsPageView.as_view(), name='graphics_page'),

    # Páginas de Login
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', views.logout, name='logout'),
    path('signup/', views.signup, name='signup'),


    # Para importar tabelas csv nos bancos (não tem acesso pela interface)
    path('import_db_users/', views.import_db_users, name='import_db_users'),
    path('import_db_friends/', views.import_db_friends, name='import_db_friends'),
    path('import_db_likes_movie/', views.import_db_likes_movie, name='import_db_likes_movie'),
    path('import_db_likes_music/', views.import_db_likes_music, name='import_db_likes_music'),




]

