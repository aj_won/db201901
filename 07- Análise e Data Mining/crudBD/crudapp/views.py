from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.views.generic import DetailView
from django.template.loader import get_template
from django.http import HttpResponse
from .resources import *
from tablib import Dataset
from django.db.models import Avg, StdDev
from django.db import connection
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
from math import pi
import base64, io
import csv
from bs4 import BeautifulSoup
import requests
import xml.etree.cElementTree as ET
from bootstrap_modal_forms.generic import BSModalLoginView, BSModalCreateView, BSModalUpdateView, BSModalDeleteView
from django.contrib.auth import login, authenticate, logout as django_logout
from .forms import LoginForm, SignUpForm, AddMovieForm, EditUserForm,AddMusicForm
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView
from .models import Movie, Music

from bs4 import BeautifulSoup
import requests
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
client_credentials_manager = SpotifyClientCredentials(client_id='f3754bb8fb9d458d8cb25591f58e77b9',
                                                               client_secret='4daeb28a153c4a03854052d7ac0a11dd')

sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

def logout(request):
    django_logout(request)
    return redirect('crudapp:main_page')

def add_friend(request, user_pk, user_pk2):
    user1 = User.objects.get(pk=user_pk)
    user2 = User.objects.get(pk=user_pk2)
    Friends.objects.create(user1=user1, user2=user2)
    return redirect('crudapp:add_friend_page', user_pk=user_pk)

def import_db_users(request):
    if request.method == 'POST':
        user_resource = UserResource()

        new_users = request.FILES['myfile']
        s = new_users.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido
            dataset.append(s_list[ind])

        result = user_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            user_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_friends(request):
    if request.method == 'POST':
        friends_resource = FriendsResource()
        new_friends = request.FILES['myfile']
        s = new_friends.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
                s_list[ind][1] = User.objects.get(link=entry[1]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = friends_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            friends_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_movie(request):
    if request.method == 'POST':
        movie_resource = LikesMovieResource()
        new_movies = request.FILES['myfile']
        s = new_movies.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = movie_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            movie_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_music(request):
    if request.method == 'POST':
        music_resource = LikesMusicResource()
        new_music = request.FILES['myfile']
        s = new_music.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = music_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            music_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('crudapp:main_page')
    else:
        form = SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

def add_movie_by_click(request):
    print(request.GET)
    return redirect('crudapp:main_page')


class UserListView(ListView):
    model = User
    template_name = 'crudapp/list_users.html'
    ordering = ['first_name']


class NewUserView(CreateView):
    form_class = SignUpForm
    template_name = 'crudapp/form_user.html'

    def post(self, request):
        form = self.get_form()
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('crudapp:user_page', username=username)
        else:
            return self.form_invalid(form)


class MainPageView(TemplateView):
    def get(self, request):
        context = {}
        if str(request.user) != 'AnonymousUser':
            self.user = request.user
            context = self.get_context_data()
        return render(request, 'crudapp/main_page.html', context)

    def get_context_data(self):
        context = {'movie_recommendations': self.get_recommended_movie(),
                   'music_recommendations': self.get_recommended_music()}
        return context

    def get_recommended_movie(self):
        user = self.user
        liked_genres = {}
        liked_movies = LikesMovie.objects.filter(user1=user)
        total_likes = len(liked_movies)
        if total_likes == 0:
            return None
        for relation in liked_movies:
            m = Movie.objects.get(pk=relation.movie_id)
            if m.genero not in liked_genres:
                liked_genres.update({m.genero: 1})
            else:
                liked_genres[m.genero] += 1

        recomendation = []
        sorted_genres = sorted(liked_genres.items(), key=lambda x: x[1])
        n = len(sorted_genres)
        if n == 0:
            recomendation = None
        elif n == 1:
            movies = Movie.objects.filter(genero=sorted_genres[0][0])  # Filmes do gênero
            not_liked_relation = LikesMovie.objects.exclude(user1=user).values('movie_id').distinct()
            not_liked_yet = movies.filter(pk__in=not_liked_relation).exclude(pk__in=liked_movies.values('movie_id'))
            recommended = not_liked_yet.exclude(pk__in=liked_movies).order_by("?").first()
            if recommended is not None:
                recomendation.append(recommended)
        elif n >= 2:
            for item in sorted_genres[-2:]:
                movies = Movie.objects.filter(genero=item[0])  # Filmes do gênero
                not_liked_relation = LikesMovie.objects.exclude(user1=user).values('movie_id').distinct()
                not_liked_yet = movies.filter(pk__in=not_liked_relation).exclude(pk__in=liked_movies.values('movie_id'))
                recommended = not_liked_yet.exclude(pk__in=liked_movies).order_by("?").first()
                if recommended is not None:
                    recomendation.append(recommended)

        return recomendation

    def get_recommended_music(self):
        user = self.user
        liked_genres = {}
        liked_music = LikesMusic.objects.filter(user1=user)
        total_likes = len(liked_music)
        if total_likes == 0:
            return None
        for relation in liked_music:
            m = Music.objects.get(pk=relation.music_id)
            if m.genero not in liked_genres:
                liked_genres.update({m.genero: 1})
            else:
                liked_genres[m.genero] += 1

        recomendation = []
        sorted_genres = sorted(liked_genres.items(), key=lambda x: x[1])
        n = len(sorted_genres)
        if n == 0:
            recomendation = None
        elif n == 1:
            music = Music.objects.filter(genero=sorted_genres[0][0])  # Músicas do gênero
            not_liked_relation = LikesMusic.objects.exclude(user1=user).values('music_id').distinct()
            not_liked_yet = music.filter(pk__in=not_liked_relation).exclude(pk__in=liked_music.values('music_id'))
            recommended = not_liked_yet.exclude(pk__in=liked_music).order_by("?").first()
            if recommended is not None:
                recomendation.append(recommended)
        elif n >= 2:
            for item in sorted_genres[-2:]:
                music = Music.objects.filter(genero=item[0])  # Filmes do gênero
                not_liked_relation = LikesMusic.objects.exclude(user1=user).values('music_id').distinct()
                not_liked_yet = music.filter(pk__in=not_liked_relation).exclude(pk__in=liked_music.values('music_id'))
                recommended = not_liked_yet.exclude(pk__in=liked_music).order_by("?").first()
                if recommended is not None:
                    recomendation.append(recommended)

        return recomendation


class EditUserView(UpdateView):
    model = User
    form_class = EditUserForm
    template_name = 'crudapp/edit_user.html'
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'
    # success_url = reverse_lazy('crudapp:user_page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['music_list'] = LikesMusic.objects.filter(user1=self.object.pk)
        context['movie_list'] = LikesMovie.objects.filter(user1=self.object.pk)
        return context

    def get_absolute_url(self):
        print(self.object.username)
        return super().get_absolute_url()


class DeleteUserView(DeleteView):
    model = User
    success_url = reverse_lazy('crudapp:user_list_page')
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'


class AddFriendView(View):
    def get(self, request, user_pk):
        all_users = User.objects.all().order_by('first_name')
        try:
            friends = [friendship.user2.pk for friendship in Friends.objects.filter(user1=user_pk)]
            friends = all_users.filter(pk__in=friends).order_by('first_name')
        except Friends.DoesNotExist:
            friends = []
        user = all_users.get(pk=user_pk)
        not_friends = []
        for other_user in all_users:
            if other_user.first_name != user.first_name and other_user not in friends:
                not_friends.append(other_user)
        user = all_users.get(pk=user_pk)
        return render(request, 'crudapp/add_friend.html', {'user': user, 'friends': friends, 'not_friends': not_friends})


class AddMusicView(CreateView):
    model = LikesMusic
    form_class = AddMusicForm
    template_name = 'crudapp/add_music.html'

    def get(self, request, user_pk, music_pk=None):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        if music_pk is not None:
            nome = Music.objects.get(pk=music_pk).nome  # ALAN, substituir aqui o link do spotify, se já não for esse.
            self.initial.update({'music_name': nome})
        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        form = self.get_form()
        self.object = None

        ### Coloquem lógica pra pegar nome da banda aqui
        # Se conseguirem achar um nome com sucesso, façam
        # link_ok = True, senão, link_ok = False
        user_name = request.POST.get('music_name', None)
        artist_name = self.retornaInfo(user_name)
        artist_name =  artist_name['artists']['items'][0]['name']
        repeated_entry = False
        try:
            music = Music.objects.get(nome=artist_name)
            if len(LikesMusic.objects.filter(user1=user_pk, music_id=music)) > 0:
                repeated_entry = True
            link_ok = True
        except Music.DoesNotExist:
            new_music = self.cadastra_music(artist_name)
            if new_music is not None:
                music = Music.objects.create(**new_music)
                music.save()
                link_ok = True
            else:
                link_ok = False
        if form.is_valid() and link_ok and not repeated_entry:
            form.instance.music = music
            return self.form_valid(form)
        else:
            if not link_ok:
                form.add_error(None, "Artista não encontrado.")
            if repeated_entry:
                form.add_error(None, "Este artista já está cadastrado na sua conta.")
            return self.form_invalid(form)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        username = form.instance.user1.username
        form.save()
        return redirect('crudapp:user_page', username=username)

    def cadastra_music(self, music_name):
        try:
            info = self.retornaInfo(music_name)
            name = info['artists']['items'][0]['name']
            genero = info['artists']['items'][0]['genres'][0]
            seguidores = info['artists']['items'][0]['followers']['total']
            spotify = info['artists']['items'][0]['external_urls']['spotify']
            img_link = info['artists']['items'][0]['images'][1]['url']  # ALAN, substitui aqui o link da imagem
            return {'nome': name, 'genero': genero, 'seguidores': seguidores, 'spotify': spotify, 'img_link': img_link}
        except:
            return None

    def retornaInfo(self, nome):
        results = sp.search(q=nome, type="artist", limit="10")
        return results


class AddMovieView(CreateView):
    model = LikesMovie
    form_class = AddMovieForm
    template_name = 'crudapp/add_movie.html'

    def get(self, request, user_pk, movie_pk=None):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        if movie_pk is not None:
            link = Movie.objects.get(pk=movie_pk).link
            self.initial.update({'movie_link': link})

        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk, movie_pk=None):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        form = self.get_form()
        self.object = None
        link = request.POST.get('movie_link', None)
        repeated_entry = False
        try:
            print(link)
            movie = Movie.objects.get(link=link)
            if len(LikesMovie.objects.filter(user1=user_pk, movie=movie)) > 0:
                repeated_entry = True
            link_ok = True
        except Movie.DoesNotExist:
            new_movie = self.cadastra_movie(link)
            if new_movie is not None:
                movie = Movie.objects.create(**new_movie)
                movie.save()
                link_ok = True
            else:
                link_ok = False
        if form.is_valid() and link_ok and not repeated_entry:
            form.instance.movie = movie
            return self.form_valid(form)
        else:
            if not link_ok:
                form.add_error(None, "Filme não encontrado.")
            if repeated_entry:
                form.add_error(None, "Este filme já está cadastrado na sua conta.")
            return self.form_invalid(form)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        username = form.instance.user1.username
        form.save()
        return redirect('crudapp:user_page', username=username)

    def cadastra_movie(self, link):
        try:
            resposta = requests.get(link)
            soup = BeautifulSoup(resposta.text, features="html.parser")
            movie_name = self.retornaNome(soup)
            # movie_ano = self.retornaAno(soup)
            movie_genero = self.retornaGenero(soup)
            movie_diretor = self.retornaDiretor(soup)
            img_link = self.retornaImgLink(soup)

            return {'nome': movie_name, 'genero': movie_genero, 'diretor': movie_diretor,
                    'img_link': img_link, 'link': link}
        except Exception as e:
            print(e)
            return None

    def retornaImgLink(self, soup):
        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'slate_wrapper'},),
                 ({'class': 'poster'},),
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            if current_info is None:
                return

        finalmente = current_info.find('img', {'class': ''}).get('src')
        if finalmente is None:
            return
        return finalmente

    def retornaNome(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            if current_info is None:
                return
            # print(current_info.encode("utf-8"), type(current_info))

        finalmente = current_info.find('h1', {'class': ''})
        if finalmente is None:
            return

        finalmente = finalmente.text
        return finalmente

    def retornaAno(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},),
                 ({'class': 'subtext'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a', {'title': 'See more release dates'}).text
        return finalmente

    def retornaGenero(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},),
                 ({'class': 'subtext'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a').text
        return finalmente

    def retornaDiretor(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'plot_summary_wrapper'},),
                 ({'class': 'plot_summary'},),
                 ({'class': 'credit_summary_item'},),
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a').text
        return finalmente


class StatisticsPageView(View):
    def get(self, request):
        return render(request, 'crudapp/statistics_page.html')


class AveragePageView(View):
    def get(self, request):
        ##### item 1 artistas #####
        notas_music = LikesMusic.objects.all().aggregate(Avg('rating'))['rating__avg']
        desvio_music = LikesMusic.objects.all().aggregate(StdDev('rating'))['rating__stddev']

        ##### item 1 filmes #####
        notas_films = LikesMovie.objects.all().aggregate(Avg('rating'))['rating__avg']
        desvio_films = LikesMovie.objects.all().aggregate(StdDev('rating'))['rating__stddev']

        return render(request, 'crudapp/average_page.html',
                      {'notas_music':notas_music, 'desvio_music':desvio_music,
                       'notas_films':notas_films, 'desvio_films':desvio_films})


class RatingsPageView(View):
    def get(self, request):
        ##### item 2 artistas #####
        info_music = []  # lista de dicionarios
        all_artistas = LikesMusic.objects.all()
        all_musicas = Music.objects.all()
        for artista in all_artistas:
            jafoi = False
            for item in info_music:
                if artista.music_id == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_artistas.filter(music_id=artista.music_id)
            if cada_um.count() > 1:
                rating = cada_um.aggregate(Avg('rating'))['rating__avg']
                for musica in all_musicas:
                    if musica.id == artista.music_id:
                        break
                info_music.append({'link': musica.nome  , 'rating': rating})
                info_music.sort(key=lambda x: x['rating'], reverse=True)

        ##### item 1 filmes #####
        info_films = []
        all_films = LikesMovie.objects.all()
        all_filmes = Movie.objects.all()
        for movie in all_films:
            jafoi = False
            for item in info_films:
                if movie.movie_id == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_films.filter(movie_id=movie.movie_id)
            if cada_um.count() > 1:
                for filme in all_filmes:
                    if filme.id == movie.movie_id:
                        break
                rating = cada_um.aggregate(Avg('rating'))['rating__avg']
                info_films.append({'link': filme.nome, 'rating': rating})
                info_films.sort(key=lambda x: x['rating'], reverse=True)

        return render(request, 'crudapp/ratings_page.html',
                  {'tabela_music': info_music,
                   'tabela_films': info_films,})


class PopularityPageView(View):
    def get(self, request):
    ##### item 3 #####
        info_populares_musica = []
        all_artistas = LikesMusic.objects.all()
        all_musicas = Music.objects.all()
        for artista in all_artistas:
            jafoi = False
            for item in info_populares_musica:
                if artista.music_id == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_artistas.filter(music_id=artista.music_id)
            count = cada_um.count()
            for musica in all_musicas:
                if musica.id == artista.music_id:
                    break
            info_populares_musica.append({'link': musica.nome, 'count': count})
            info_populares_musica.sort(key=lambda x: x['count'], reverse=True)
        info_populares_musica = info_populares_musica[0:10]

    ##### item 3 filmes #####
        info_populares_films = []
        all_movies = LikesMovie.objects.all()
        all_filmes = Movie.objects.all()

        for movie in all_movies:
            jafoi = False
            for item in info_populares_films:
                if movie.movie_id == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_movies.filter(movie_id=movie.movie_id)
            count = cada_um.count()
            for filme in all_filmes:
                if filme.id == movie.movie_id:
                    break
            info_populares_films.append({'link': filme.nome, 'count': count})
            info_populares_films.sort(key=lambda x: x['count'], reverse=True)
        info_populares_films = info_populares_films[0:10]

        return render(request, 'crudapp/popularity_page.html', {'populares_musica':info_populares_musica,'populares_filmes':info_populares_films})


class ConhecidosPageView(View):
    def get(self, request):
        ##### item 5 #####
        cursor = connection.cursor()

        sql_string = 'SELECT cn.user1, cn.user2 FROM crud.conhece_normalizada cn ' \
                     'WHERE (SELECT COUNT(cf1.movie_link) FROM crud.likes_movie cf1, crud.likes_movie cf2, crud.conhece_normalizada cn2 ' \
                     'WHERE cn.user1 = cf1.user1_id AND cn.user2=cf2.user1_id AND cf1.movie_link = cf2.movie_link)>0 ' \
                     'ORDER by (SELECT COUNT(cf1.movie_link) FROM crud.likes_movie cf1, crud.likes_movie cf2, crud.conhece_normalizada cn' \
                     ' WHERE cn.user1=cf1.user1_id AND cn.user2=cf2.user1_id AND cf1.movie_link=cf2.movie_link) DESC LIMIT 1'
        cursor.execute(sql_string)
        users_films = cursor.fetchall()
        users_films = User.objects.filter(pk__in=users_films[0])

        ##### item 6 #####

        sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
                     'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 42)'
        cursor.execute(sql_string)
        count_con_con_A = cursor.fetchall()[0][0]
        sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
                     'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 47)'
        cursor.execute(sql_string)
        count_con_con_C = cursor.fetchall()[0][0]
        sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
                     'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 62)'
        cursor.execute(sql_string)
        count_con_con_J = cursor.fetchall()[0][0]

        return render(request, 'crudapp/conhecidos_page.html',
                      {'users_films': users_films, 'conhecidos_conhecidos_A': count_con_con_A,
                       'conhecidos_conhecidos_C': count_con_con_C, 'conhecidos_conhecidos_J': count_con_con_J})


class GraphicsPageView(View):
    def get(self, request):

        ##### item 7 #####
        cursor = connection.cursor()
        sql_string = 'SELECT COUNT(lm.user1_id) FROM crud.likes_movie lm GROUP BY lm.user1_id ORDER BY COUNT(lm.user1_id) DESC '
        cursor.execute(sql_string)
        qtd = cursor.fetchall()
        z = [0] * 20
        for i in range(0, len(qtd)):
            a = qtd[i][0]
            z[a] = z[a] + 1

        x = np.arange(0, len(z), 1)  # Domínio da função - [start, end, step]
        y = z  # Função
        fig, ax = plt.subplots()
        plt.plot(x, y)
        ax.set(xlabel='Número de pessoas', ylabel='Filmes curtidos', title="Número de pessoas que curtiram exatamente x filmes")
        ax.grid()

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img3 = base64.b64encode(buf.getvalue())
        img3 = img3.decode()

        ##### ITEM 8 #####

        contagens_filmes = []
        all_filmes = LikesMovie.objects.all()
        for filme in all_filmes:
            cada_filme = all_filmes.filter(movie_link=filme.movie_link)
            contagens_filmes.append(cada_filme.count())

        eixo_y = []
        for x in range(len(contagens_filmes)):
            quantidade = contagens_filmes.count(contagens_filmes[x])
            eixo_y.append(quantidade)
        sem_repetidos = []
        for i in eixo_y:
            if i not in sem_repetidos:
                sem_repetidos.append(i)

        x = np.arange(0, 12, 1)  # Domínio da função - [start, end, step]
        y = sem_repetidos  # Função
        print(x, '\n', y)
        fig, ax = plt.subplots()
        plt.bar(x, y)
        ax.set(xlabel='Número de filmes', ylabel='Número de pessoas', title="Número de filmes curtidos por exatamente x pessoas")
        ax.grid()
        # plt.show()  # Descomentem pra ver o gráfico fora do site

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img = base64.b64encode(buf.getvalue())
        img = img.decode()

        #### ITEM 9 ########
        conhecem = {}
        users = User.objects.all()
        max_friends = 0
        for user in users:
            number_of_friends = len(Friends.objects.filter(user1=user.pk))
            if number_of_friends > max_friends:
                max_friends = number_of_friends

        for i in range(max_friends+1):
            conhecem.update({i: 0})

        for user in users:
            number_of_friends = len(Friends.objects.filter(user1=user.pk))
            conhecem[number_of_friends] += 1

        x = list(conhecem.keys())   # Domínio da função - [start, end, step]
        y = list(conhecem.values())
        fig, ax = plt.subplots()
        plt.bar(x, y)
        ax.set(xlabel='Amigos', ylabel='Pessoas com esse número de amigos', title="Número de pessoas com a mesma quantidade de amigos")
        ax.grid()

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img2 = base64.b64encode(buf.getvalue())
        img2 = img2.decode()

        result = get_template('crudapp/graphics_page.html').render({'img': img, 'img2': img2, 'img3': img3})
        return HttpResponse(result)


class LoginView(BSModalLoginView):
    authentication_form = LoginForm
    template_name = 'registration/login.html'

    def post(self, request):
        super().post(request)
        print("caralho")
        return redirect('crudapp:new_user_page')


class ShowUserView(DetailView):
    model = User
    template_name = 'crudapp/user_show.html'
    slug_url_kwarg = 'username'
    slug_field = 'username'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()

        likedmovies = LikesMovie.objects.filter(user1_id=user.pk)
        movie_list = Movie.objects.filter(likesmovie__in=likedmovies)

        for movie in movie_list:
            movie.rating = LikesMovie.objects.get(user1_id=user.pk, movie_id=movie.pk).rating

        likedmusics = LikesMusic.objects.filter(user1_id=user.pk)
        music_list = Music.objects.filter(likesmusic__in=likedmusics)
        for music in music_list:
            music.rating = LikesMusic.objects.get(user1_id=user.pk, music_id=music.pk).rating

        # seeds = []
        # for music in music_list:
        #     music.rating = LikesMusic.objects.get(user1_id=user.pk, music_id=music.pk).rating
        #     achou = -1
        #     achou = music.spotify.find("artist/")
        #     if achou != -1:
        #         artist_name = music.spotify[achou+7:]
        #
        #     if len(seeds) < 5:
        #         seeds.append(artist_name)
        #
        # music_recommendaton = self.retornaRecommendSpotify(seeds)
        # for i in music_recommendaton:
        #     for music in music_list:
        #         if i == music.nome:
        #             music_recommendaton.remove(i)

        context.update({'music_list': music_list, 'movie_list': movie_list,})
                        # 'music_recommendaton': music_recommendaton})
        return context

    def retornaNome(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            if current_info is None:
                return
            # print(current_info.encode("utf-8"), type(current_info))

        finalmente = current_info.find('h1', {'class': ''})
        if finalmente is None:
            return

        finalmente = finalmente.text
        return finalmente

    def retornaAno(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},),
                 ({'class': 'subtext'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a', {'title': 'See more release dates'}).text
        return finalmente

    def retornaGenero(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'vital'},),
                 ({'class': 'title_block'},),
                 ({'class': 'title_bar_wrapper'},),
                 ({'class': 'title_wrapper'},),
                 ({'class': 'subtext'},)
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a').text
        return finalmente

    def retornaDiretor(self, soup):

        lista = [({'id': 'wrapper'}, {'class': 'redesign'}),
                 ({'id': 'root'}, {'class': 'redesign'}),
                 ({'id': 'pagecontent'}, {'class': 'pagecontent'}),
                 ({'id': 'content-2-wide'}, {'class': 'flatland'}),
                 ({'id': 'main_top'}, {'class': 'main'}),
                 ({'class': 'title-overview'},),
                 ({'id': 'title-overview-widget'}, {'class': 'heroic-overview'}),
                 ({'class': 'plot_summary_wrapper'},),
                 ({'class': 'plot_summary'},),
                 ({'class': 'credit_summary_item'},),
                 ]

        current_info = soup.find('body', {'class': 'fixed'})
        if current_info is None:
            return

        for arg in lista:
            current_info = current_info.find('div', *arg)
            #
            if current_info is None:
                return None

        finalmente = current_info.find('a').text
        return finalmente

    def retornaRecommendSpotify(self, seeds):
        recommendations = sp.recommendations(seeds, limit=10)
        rec_names = []
        rec_names.append(recommendations['tracks'][0]['artists'][0]['name'])
        rec_names.append(recommendations['tracks'][1]['artists'][0]['name'])
        rec_names.append(recommendations['tracks'][2]['artists'][0]['name'])
        rec_names.append(recommendations['tracks'][3]['artists'][0]['name'])
        rec_names.append(recommendations['tracks'][4]['artists'][0]['name'])
        rec_names.append(recommendations['tracks'][5]['artists'][0]['name'])
        rec_names.append(recommendations['tracks'][6]['artists'][0]['name'])
        rec_names.append(recommendations['tracks'][7]['artists'][0]['name'])

        return rec_names


