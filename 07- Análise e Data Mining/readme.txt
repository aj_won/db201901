Guia

Para acessar o site, é necessário ter o seguinte instalado:
(podem haver mais dependências, essas são as principais)

- Python 3.5
- Django 2.1
- psycopg2
- django-import-export


Para colocar o servidor online:

- Pelo terminal, acesse a pasta "../05 - Cadastro - CRUD/crudBD".
- Digite o comando "python manage.py runserver".
- Pelo navegador, acesse "127.0.0.1:8000/crud".


Dentro da pasta "../05 - Cadastro - CRUD/crudBD/crudapp" estão localizados os seguintes arquivos:

- models.py - Este arquivo guarda as definições dos modelos. É daí que o Django tira as informações pra criar
			  e gerenciar as tabelas.
- urls.py - Este arquivo define a estrutura do site e as views correspondentes que são chamadas com cada url.
- views.py - Este arquivo define todas as views. As views são o backend de cada página... tratam requisições
			 GET/POST e acessam o banco buscando informações dos modelos pra construção da página. Além disso,
			 enviam as informações para um template, para que os dados possam ser mostrados na tela (frontend).
			 Views normalmente são classes, mas podem ser funções para requisições mais simples.
- forms.py - Este arquivo gera os forms usados nas views. Os forms podem ser construídos manualmente, mas
			 geralmente usam algum Model de base. Alguns forms que não requerem lógica extra são gerados
			 automaticamente por views específicas, e não precisam ser declarados.


Dentro da pasta "../05 - Cadastro - CRUD/crudBD/crudapp/templates/crudapp" estão localizados os templates.
Templates são páginas html usadas como frontend. O Django usa a Django Template Language (DTL) para inserir
dados de forma dinâmica nos arquivos html.
A sintaxe básica é a seguinte:

- {% comando argumento %} - Executa um comando da linguagem com o argumento passado.
- {{ objeto.atributo }} - Se a View passar um objeto para o template, é possível acessá-lo dessa forma.

Exemplos:
{% extends 'crudapp/generic_base.html' %} - Usa generic_base.html de base para construir a página, substituindo
											 os blocos {% block nome_do_bloco %} da página pai pelo conteúdo dos
											 blocos correspondentes da página filho.

{% for obj in obj_list %} - Loop for em DTL
	<p>obj.nome</p>
{% empty %} - Caso a lista esteja vazia ou o objeto em questão não exista, ele coloca o que estiver aqui
	<p>Nada aqui.</p>
{% endfor %}

{% if condição %} - Condicional em DTL
	<p>Alguma coisa.</p>
{% elif condição %}
	<p>Outra coisa.</p>
{% else %}
	<p>Mais uma coisa.</p>
{% endif %}