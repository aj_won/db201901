# Estrutura básica do trabalho de CSB30 - Introdução A Bancos De Dados

Modifique este aquivo README.md com as informações adequadas sobre o seu grupo.

## Integrantes do grupo

Liste o nome, RA e o usuário GitLab para cada integrante do grupo.

- Alan Jun Kowa Onnoda, 1936328, aj_won
- Caroline Rosa, 1904957, caroles
- Jordão Soviersovski, 1236822, Jord_Hisu

## Descrição da aplicação a ser desenvolvida 

Descreva aqui uma visão geral da aplicação que será desenvolvida pelo grupo durante o semestre. **Este texto deverá ser escrito quando requisitado pelo professor.** O conteúdo vai evoluir à medida em que o grupo avança com a implementação.

- O que vocês vão implementar: O projeto final consiste em uma rede social onde você pode adicionar seus filmes e artistas musicais de interesse, além de adicionar amigos e ter acesso aos interesses deles. Além disso, pode receber recomendações de filmes/músicas com base em seus interesses.


- Quais tecnologias vão utilizar: O coração do projeto: o framework web Django para python, além de um algoritmo de machine learning para as recomendações e o próprio servidor disponibilzado pelo professor para armazenar o banco de dados.


- Por que decidiram pelo tema: Por que há a possibilidade de dar continuidade no que já vem sendo trabalhado e estudado pela equipe desde os últimos trabalhos, proporcionando a chance de aprimoramento e aprofundamento no assunto.


- Quais os desafios relacionados com bancos de dados (consultas, modelos, etc.): Fazer funcionar as tabelas criadas em diferentes schemas em conjunto com o django.


- Quais os desafios relacionados com suas áreas de interesse: Adquirir e aprimorar conhecimentos sobre a linguagem python e o funcionamento e uso do framework django.


- Quais conhecimentos novos vocês pretendem adquirir: Uso e integração de um algoritmo de machine learning para realizar recomendações para o usuário da rede social com base em seus gostos pessoais e nos gostos de seus amigos.


- O que vocês já produziram (protótipos, esquemas de telas, teste de codificação de partes críticas, etc.): Grande parte das telas, página de login, o banco de dados da rede social, extração das informações de filmes e músicas de acervos da internet.


- Nome: Alan Jun Kowa Onnoda

- Qual o seu foco na implementação do trabalho? Utilizar e aplicar conhecimentos de extração e tratamento de dados obtidos em aula.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais? Sim, api e Django, que foram utilizadas durante os trabalhos anteriores da disciplina.

- Qual aspecto do trabalho te interessa mais? Dados sociais relacionados a música.


- Nome: Caroline Rosa da Silva

- Qual o seu foco na implementação do trabalho? Fazer o planejamento das páginas e tabelas da rede social e trabalhar na extração de dados.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais? Sim, Django, com base na utilização nos trabalhos feitos ao decorrer do semestre.

- Qual aspecto do trabalho te interessa mais? O funcionamento e diversas funcionalidades do Django. 


- Nome: Jordão Victor Soviersovski

- Qual o seu foco na implementação do trabalho? Implementação do CRUD e interfaces com o Django.

- Já teve alguma experiência com as tecnologias que serão usadas no trabalho? Quais? Sim, Django e python com base nos trabalhos do semestre e experiências anteriores.

- Qual aspecto do trabalho te interessa mais? A implementação e funcionamento do algoritmo de machine learning.