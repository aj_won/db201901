from .models import User
from django.forms import ModelForm


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = '__all__'


class EditUserForm(UserForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['link'].widget.attrs['readonly'] = True

    def clean_link(self):
        instance = getattr(self, 'instance', None)
        if instance:
            return instance.link
        else:
            return self.cleaned_data.get('link', None)