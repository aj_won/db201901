from import_export import resources
from .models import User, Friends, LikesMovie, LikesMusic

class UserResource(resources.ModelResource):
    class Meta:
        model = User

class FriendsResource(resources.ModelResource):
    class Meta:
        model = Friends

class LikesMovieResource(resources.ModelResource):
    class Meta:
        model = LikesMovie

class LikesMusicResource(resources.ModelResource):
    class Meta:
        model = LikesMusic