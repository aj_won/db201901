from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic.list import ListView
from django.views.generic.edit import FormView, CreateView, UpdateView, DeleteView
from django.template.loader import get_template
from django.http import HttpResponse
from .models import User, Friends
from .forms import EditUserForm
from .resources import *
from tablib import Dataset
from django.db.models import Avg, StdDev
from django.db import connection
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import base64, io

from bs4 import BeautifulSoup
import requests
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
client_credentials_manager = SpotifyClientCredentials(client_id='f3754bb8fb9d458d8cb25591f58e77b9',
                                                              client_secret='4daeb28a153c4a03854052d7ac0a11dd')

sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)


class UserListView(ListView):
    model = User
    template_name = 'crudapp/list_users.html'
    ordering = ['nome']


class NewUserView(CreateView):
    model = User
    fields = '__all__'
    template_name = 'crudapp/form_user.html'
    success_url = reverse_lazy('crudapp:main_page')


class MainPageView(View):
    def get(self, request):
        return render(request, 'crudapp/main_page.html', {})


class EditUserView(UpdateView):
    model = User
    form_class = EditUserForm
    template_name = 'crudapp/edit_user.html'
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'
    success_url = reverse_lazy('crudapp:user_list_page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['music_list'] = LikesMusic.objects.filter(user1=self.object.pk)
        context['movie_list'] = LikesMovie.objects.filter(user1=self.object.pk)
        return context


class DeleteUserView(DeleteView):
    model = User
    success_url = reverse_lazy('crudapp:user_list_page')
    slug_field = 'pk'
    slug_url_kwarg = 'user_pk'


class AddFriendView(View):
    def get(self, request, user_pk):
        all_users = User.objects.all().order_by('nome')
        try:
            friends = [friendship.user2.pk for friendship in Friends.objects.filter(user1=user_pk)]
            friends = all_users.filter(pk__in=friends).order_by('nome')
        except Friends.DoesNotExist:
            friends = []
        user = all_users.get(pk=user_pk)
        not_friends = []
        for other_user in all_users:
            if other_user.link != user.link and other_user not in friends:
                not_friends.append(other_user)
        user = all_users.get(pk=user_pk)
        return render(request, 'crudapp/add_friend.html', {'user': user, 'friends': friends, 'not_friends': not_friends})


class AddMusicView(CreateView):
    model = LikesMusic
    fields = ('band_link', 'rating')
    template_name = 'crudapp/add_music.html'

    def get(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        return super().post(request)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        form.save()
        return redirect('crudapp:edit_user_page', user_pk=self.user_pk)


class AddMovieView(CreateView):
    model = LikesMovie
    fields = ('movie_link', 'rating')
    template_name = 'crudapp/add_movie.html'

    def get(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'get_context_data'
        self.user_pk = user_pk
        return super().get(request)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        # Inserimos o conteúdo de user aqui para podermos acessar no template.
        data['user'] = User.objects.get(pk=self.user_pk)
        return data

    def post(self, request, user_pk):
        # Inserimos self.user_pk pra que consigamos acessar de 'form_valid'
        self.user_pk = user_pk
        return super().post(request)

    def form_valid(self, form):
        form.instance.user1 = User.objects.get(pk=self.user_pk)
        form.save()
        return redirect('crudapp:edit_user_page', user_pk=self.user_pk)


def add_friend(request, user_pk, user_pk2):
    user1 = User.objects.get(pk=user_pk)
    user2 = User.objects.get(pk=user_pk2)
    Friends.objects.create(user1=user1, user2=user2)
    return redirect('crudapp:add_friend_page', user_pk=user_pk)

def import_db_users(request):
    if request.method == 'POST':
        user_resource = UserResource()

        new_users = request.FILES['myfile']
        s = new_users.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido
            dataset.append(s_list[ind])

        result = user_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            user_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_friends(request):
    if request.method == 'POST':
        friends_resource = FriendsResource()
        new_friends = request.FILES['myfile']
        s = new_friends.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
                s_list[ind][1] = User.objects.get(link=entry[1]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = friends_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            friends_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_movie(request):
    if request.method == 'POST':
        movie_resource = LikesMovieResource()
        new_movies = request.FILES['myfile']
        s = new_movies.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = movie_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            movie_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')

def import_db_likes_music(request):
    if request.method == 'POST':
        music_resource = LikesMusicResource()
        new_music = request.FILES['myfile']
        s = new_music.read().decode("utf-8")
        s_list = s.split('\r')
        header = s_list.pop(0)  # Header
        header += ',id'  # Adiciona id ao header (necessário)
        s_list.pop()  # '\n' (lixo)
        dataset = Dataset(headers=header.split(','))
        for ind, entry in enumerate(s_list):
            s_list[ind] = entry.replace('\n', '').split(',')
            s_list[ind].append('')  # Para o campo id que será preenchido

        for ind, entry in enumerate(s_list):
            try:
                s_list[ind][0] = User.objects.get(link=entry[0]).pk
            except User.DoesNotExist:
                continue
            dataset.append(s_list[ind])

        result = music_resource.import_data(dataset, dry_run=True, raise_errors=True)  # Test the data import
        print("HAS ERRORS:", result.has_errors())

        if not result.has_errors():
            music_resource.import_data(dataset, dry_run=False)  # Actually import now

    return render(request, 'crudapp/import_db.html')


class StatisticsPageView(View):
    def get(self, request):
        return render(request, 'crudapp/statistics_page.html')

class AveragePageView(View):
    def get(self, request):
        artists = LikesMusic.objects.all()
        for artist in artists:
            url_wiki = artist.band_link
            resposta = requests.get(url_wiki)
            soup_music = BeautifulSoup(resposta.text, features="html.parser")
            artist_name = soup_music.find('h1', class_='firstHeading').text
            achou = artist_name.find("(")
            if achou != -1:
                artist_name = artist_name[:achou - 1]
            results = sp.search(q=artist_name, type="artist", limit="10")
            print(results['artists']['items'][0]['name'])

        artist_name = soup_music.find('h1', class_='firstHeading').text

        ##### item 1 filmes #####
        notas_films = LikesMovie.objects.all().aggregate(Avg('rating'))['rating__avg']
        desvio_films = LikesMovie.objects.all().aggregate(StdDev('rating'))['rating__stddev']

        return render(request, 'crudapp/average_page.html',
                      {'notas_music':notas_music, 'desvio_music':desvio_music,
                       'notas_films':notas_films, 'desvio_films':desvio_films})

class RatingsPageView(View):
    def get(self, request):
        ##### item 2 artistas #####
        info_music = []  # lista de dicionarios
        all_artistas = LikesMusic.objects.all()
        for artista in all_artistas:
            jafoi = False
            for item in info_music:
                if artista.band_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_artistas.filter(band_link=artista.band_link)
            if cada_um.count() > 1:
                rating = cada_um.aggregate(Avg('rating'))['rating__avg']
                info_music.append({'link': artista.band_link, 'rating': rating})
                info_music.sort(key=lambda x: x['rating'], reverse=True)

        ##### item 1 filmes #####
        info_films = []
        all_films = LikesMovie.objects.all()
        for movie in all_films:
            jafoi = False
            for item in info_films:
                if movie.movie_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_films.filter(movie_link=movie.movie_link)
            if cada_um.count() > 1:
                rating = cada_um.aggregate(Avg('rating'))['rating__avg']
                info_films.append({'link': movie.movie_link, 'rating': rating})
                info_films.sort(key=lambda x: x['rating'], reverse=True)

        return render(request, 'crudapp/ratings_page.html',
                  {'tabela_music': info_music,
                   'tabela_films': info_films,})


class PopularityPageView(View):
    def get(self, request):
    ##### item 3 #####
        info_populares_musica = []
        all_artistas = LikesMusic.objects.all()
        for artista in all_artistas:
            jafoi = False
            for item in info_populares_musica:
                if artista.band_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_artistas.filter(band_link=artista.band_link)
            count = cada_um.count()
            info_populares_musica.append({'link': artista.band_link, 'count': count})
            info_populares_musica.sort(key=lambda x: x['count'], reverse=True)
        info_populares_musica = info_populares_musica[0:10]

    ##### item 3 filmes #####
        info_populares_films = []
        all_movies = LikesMovie.objects.all()
        for movie in all_movies:
            jafoi = False
            for item in info_populares_films:
                if movie.movie_link == item['link']:
                    jafoi = True
                    break
            if jafoi: continue
            cada_um = all_movies.filter(movie_link=movie.movie_link)
            count = cada_um.count()
            info_populares_films.append({'link': movie.movie_link, 'count': count})
            info_populares_films.sort(key=lambda x: x['count'], reverse=True)
        info_populares_films = info_populares_films[0:10]

        return render(request, 'crudapp/popularity_page.html', {'populares_musica':info_populares_musica,'populares_filmes':info_populares_films})


# class ConhecidosPageView(View):
#     def get(self, request):
#         ##### item 5 #####
#         cursor = connection.cursor()
#
#         sql_string = 'SELECT cn.user1, cn.user2 FROM crud.conhece_normalizada cn ' \
#                      'WHERE (SELECT COUNT(cf1.movie_link) FROM crud.likes_movie cf1, crud.likes_movie cf2, crud.conhece_normalizada cn2 ' \
#                      'WHERE cn.user1 = cf1.user1_id AND cn.user2=cf2.user1_id AND cf1.movie_link = cf2.movie_link)>0 ' \
#                      'ORDER by (SELECT COUNT(cf1.movie_link) FROM crud.likes_movie cf1, crud.likes_movie cf2, crud.conhece_normalizada cn' \
#                      ' WHERE cn.user1=cf1.user1_id AND cn.user2=cf2.user1_id AND cf1.movie_link=cf2.movie_link) DESC LIMIT 1'
#         cursor.execute(sql_string)
#         users_films = cursor.fetchall()
#         users_films = User.objects.filter(pk__in=users_films[0])
#
#         ##### item 6 #####
#
#         sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
#                      'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 42)'
#         cursor.execute(sql_string)
#         count_con_con_A = cursor.fetchall()[0][0]
#         sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
#                      'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 47)'
#         cursor.execute(sql_string)
#         count_con_con_C = cursor.fetchall()[0][0]
#         sql_string = 'SELECT COUNT(cn.user2) FROM crud.conhece_normalizada cn WHERE cn.user2 ' \
#                      'IN (SELECT cn.user2 FROM crud.conhece_normalizada cn WHERE cn.user1 = 62)'
#         cursor.execute(sql_string)
#         count_con_con_J = cursor.fetchall()[0][0]
#
#         return render(request, 'crudapp/conhecidos_page.html',
#                       {'users_films': users_films, 'conhecidos_conhecidos_A': count_con_con_A,
#                        'conhecidos_conhecidos_C': count_con_con_C, 'conhecidos_conhecidos_J': count_con_con_J})

# class Graphics_1PageView(View):
#     def get(self, request):
#
#         ##### item 7 #####
#         cursor = connection.cursor()
#         sql_string = 'SELECT COUNT(lm.user1_id) FROM crud.likes_movie lm GROUP BY lm.user1_id ORDER BY COUNT(lm.user1_id) DESC '
#         cursor.execute(sql_string)
#         qtd = cursor.fetchall()
#         z = [0]*20
#         for i in range(0, len(qtd)):
#             a = qtd[i][0]
#             z[a] = z[a]+1
#
#         x = np.arange(0, len(z), 1)  # Domínio da função - [start, end, step]
#         y = z   # Função
#         fig, ax = plt.subplots()
#         plt.plot(x, y)
#         ax.set(xlabel='N de pessoas', ylabel='Filmes curtidos', title="Item 7")
#         ax.grid()
#         #plt.show()  # Descomentem pra ver o gráfico fora do site
#
#         buf = io.BytesIO()
#         plt.savefig(buf, format='png')
#         plt.close(fig)
#         img3 = base64.b64encode(buf.getvalue())
#         img3 = img3.decode()
#         result = get_template('crudapp/graphics_1_page.html').render({'img3': img3})
#         return HttpResponse(result)

class GraphicsPageView(View):
    def get(self, request):

        ##### item 7 #####
        cursor = connection.cursor()
        sql_string = 'SELECT COUNT(lm.user1_id) FROM crud.likes_movie lm GROUP BY lm.user1_id ORDER BY COUNT(lm.user1_id) DESC '
        cursor.execute(sql_string)
        qtd = cursor.fetchall()
        z = [0] * 20
        for i in range(0, len(qtd)):
            a = qtd[i][0]
            z[a] = z[a] + 1

        x = np.arange(0, len(z), 1)  # Domínio da função - [start, end, step]
        y = z  # Função
        fig, ax = plt.subplots()
        plt.plot(x, y)
        ax.set(xlabel='Número de pessoas', ylabel='Filmes curtidos', title="Número de pessoas que curtiram exatamente x filmes")
        ax.grid()

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img3 = base64.b64encode(buf.getvalue())
        img3 = img3.decode()

        ##### ITEM 8 #####

        contagens_filmes = []
        all_filmes = LikesMovie.objects.all()
        for filme in all_filmes:
            cada_filme = all_filmes.filter(movie_link=filme.movie_link)
            contagens_filmes.append(cada_filme.count())

        eixo_y = []
        for x in range(len(contagens_filmes)):
            quantidade = contagens_filmes.count(contagens_filmes[x])
            eixo_y.append(quantidade)
        sem_repetidos = []
        for i in eixo_y:
            if i not in sem_repetidos:
                sem_repetidos.append(i)

        x = np.arange(0, 12, 1)  # Domínio da função - [start, end, step]
        y = sem_repetidos  # Função
        print(x, '\n', y)
        fig, ax = plt.subplots()
        plt.bar(x, y)
        ax.set(xlabel='Número de filmes', ylabel='Número de pessoas', title="Número de filmes curtidos por exatamente x pessoas")
        ax.grid()
        # plt.show()  # Descomentem pra ver o gráfico fora do site

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img = base64.b64encode(buf.getvalue())
        img = img.decode()

        #### ITEM 9 ########
        conhecem = {}
        users = User.objects.all()
        max_friends = 0
        for user in users:
            number_of_friends = len(Friends.objects.filter(user1=user.pk))
            if number_of_friends > max_friends:
                max_friends = number_of_friends

        for i in range(max_friends+1):
            conhecem.update({i: 0})

        for user in users:
            number_of_friends = len(Friends.objects.filter(user1=user.pk))
            conhecem[number_of_friends] += 1

        x = list(conhecem.keys())   # Domínio da função - [start, end, step]
        y = list(conhecem.values())
        fig, ax = plt.subplots()
        plt.bar(x, y)
        ax.set(xlabel='Amigos', ylabel='Pessoas com esse número de amigos', title="Número de pessoas com a mesma quantidade de amigos")
        ax.grid()

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        plt.close(fig)
        img2 = base64.b64encode(buf.getvalue())
        img2 = img2.decode()

        result = get_template('crudapp/graphics_page.html').render({'img': img, 'img2': img2, 'img3': img3})
        return HttpResponse(result)


